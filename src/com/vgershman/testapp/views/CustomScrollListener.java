package com.vgershman.testapp.views;

import android.view.View;
import android.widget.AbsListView;

/**
 * Created by vgershman on 04.12.14.
 * Class that smoothly show/hide view depending on scroll amount(absolute)
 */
public class CustomScrollListener implements AbsListView.OnScrollListener {

    private View mViewToHide;
    private int mPreviousFirstVisible = 0;

    public CustomScrollListener(View view) {
        mViewToHide = view;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        changeAlpha(firstVisibleItem - mPreviousFirstVisible, totalItemCount - visibleItemCount);
        mPreviousFirstVisible = firstVisibleItem;
    }

    private void changeAlpha(int difference, int unvisibleItemCount) {
        float current = mViewToHide.getAlpha();
        float change = difference * 1.f / unvisibleItemCount;
        current -= change;
        if (current >= 1.0f) {
            current = 1.0f;
        }
        if (current <= 0) {
            current = 0.f;
        }
        mViewToHide.setAlpha(current);
    }
}
