package com.vgershman.testapp;

/**
 * Created by vgershman on 04.12.14.
 * <p/>
 * It is ok to put all static info of this small app in common interface
 */
public interface Config {

    public static final int IMAGES_COUNT = 50; //per loading
    public static final String BASE_URL = "http://placekitten.com/g/";
    public static int MIN_SIDE_SIZE = 100; //min size in px for height or width of an image
    public static int MAX_SIDE_SIZE = 1024; //max size in px for height or width of an image
}
