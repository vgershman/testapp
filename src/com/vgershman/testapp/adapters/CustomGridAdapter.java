package com.vgershman.testapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.etsy.android.grid.util.DynamicHeightImageView;
import com.vgershman.testapp.R;
import com.vgershman.testapp.data.DataManager;
import com.vgershman.testapp.data.model.ImageItem;

/**
 * Created by vgershman on 04.12.14.
 * <p/>
 * Kind of ArrayAdapter, but array is 'hidden' in DataManager class.
 */
public class CustomGridAdapter extends BaseAdapter {

    private Context mContext;

    public CustomGridAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return DataManager.getImages(mContext).size();
    }

    @Override
    public ImageItem getItem(int position) {
        return DataManager.getImages(mContext).get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.grid_item, null, false);
            viewHolder = new ViewHolder();
            viewHolder.aQuery = new AQuery(view);
            viewHolder.ivImage = (DynamicHeightImageView) view.findViewById(R.id.iv_image);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        final ViewHolder vh_ = viewHolder;
        ImageItem imageItem = getItem(position);
        /**
         * Android Query is used for image loading
         * https://code.google.com/p/android-query/#Image_Loading
         */
        viewHolder.aQuery.id(R.id.iv_image).progress(R.id.progress).image(imageItem.getUrl(), true, true, 0, R.drawable.ic_launcher, new BitmapAjaxCallback() {
            @Override
            public BitmapAjaxCallback preset(Bitmap preset) {
                return super.preset(null);
            }

            @Override
            protected void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status) {
                super.callback(url, iv, bm, status);
                double ratio = bm.getHeight() * 1.f / bm.getWidth();
                vh_.ivImage.setHeightRatio(ratio);
            }
        });
        return view;
    }

    /**
     * ViewHolder pattern
     * http://developer.android.com/training/improving-layouts/smooth-scrolling.html
     */
    static class ViewHolder {
        DynamicHeightImageView ivImage;
        AQuery aQuery;
    }
}
