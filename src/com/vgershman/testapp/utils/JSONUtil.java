package com.vgershman.testapp.utils;

import com.google.gson.Gson;
import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by vgershman on 02.02.14.
 * Gson wrapper
 */
public class JSONUtil {

    private static Gson ourInstance;

    private static Gson getInstance() {
        if (ourInstance == null) {
            ourInstance = new Gson();
        }
        return ourInstance;
    }

    public static <T> T parse(String json, Class clazz) {
        return (T) getInstance().fromJson(json, clazz);
    }

    public static <T> ArrayList<T> parseList(JSONArray jsonArray, Class clazz) {
        ArrayList<T> resultList = new ArrayList<T>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                resultList.add(JSONUtil.<T>parse(jsonArray.optJSONObject(i).toString(), clazz));
            } catch (Exception ex) {
            }
        }
        return resultList;
    }

    public static <T> String toString(ArrayList<T> list) {
        return getInstance().toJson(list);
    }
}
