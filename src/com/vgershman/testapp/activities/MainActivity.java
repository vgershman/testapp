package com.vgershman.testapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.etsy.android.grid.StaggeredGridView;
import com.vgershman.testapp.R;
import com.vgershman.testapp.adapters.CustomGridAdapter;
import com.vgershman.testapp.data.DataManager;
import com.vgershman.testapp.views.CustomScrollListener;

/**
 * Main screen of the app
 */
public class MainActivity extends Activity {

    private StaggeredGridView mGridImages;
    private Button mBtnReload;
    private CustomGridAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initViews();
        initData();
    }

    private void initViews() {
        mGridImages = (StaggeredGridView) findViewById(R.id.gv_images);
        mBtnReload = (Button) findViewById(R.id.btn_reload);
        mBtnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reload();
            }
        });
        mGridImages.setOnScrollListener(new CustomScrollListener(findViewById(R.id.container)));
    }

    private void reload() {
        DataManager.reloadImages(this);
        mAdapter.notifyDataSetChanged();
    }

    private void initData() {
        mAdapter = new CustomGridAdapter(this);
        mGridImages.setAdapter(mAdapter);
    }
}
