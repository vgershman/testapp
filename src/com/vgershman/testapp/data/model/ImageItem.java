package com.vgershman.testapp.data.model;

import com.vgershman.testapp.Config;

import java.util.Random;

/**
 * Created by vgershman on 04.12.14.
 */
public class ImageItem {

    private String mUrl;

    //"self-builder"
    public static ImageItem createFromRandom(Random random) {
        String url = Config.BASE_URL + getRandomSideSize(random) + "/" + getRandomSideSize(random);
        return new ImageItem(url);
    }

    //should be no way to create instance without validation.
    private ImageItem(String url) {
        mUrl = url;
    }

    public String getUrl() {
        return mUrl;
    }

    private static int getRandomSideSize(Random random) {
        return random.nextInt(Config.MAX_SIDE_SIZE - Config.MIN_SIDE_SIZE) + Config.MIN_SIDE_SIZE;
    }
}
