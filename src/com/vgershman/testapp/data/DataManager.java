package com.vgershman.testapp.data;

import android.content.Context;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import com.vgershman.testapp.Config;
import com.vgershman.testapp.data.model.ImageItem;
import com.vgershman.testapp.utils.JSONUtil;
import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * Created by vgershman on 04.12.14.
 */

/**
 * This class manage list of images.
 * "Simple" singleton, but ok for this app.
 */
public class DataManager {

    private static final String DATA_FIELD = "data";

    private static DataManager sInstance;
    private List<ImageItem> mImages;

    public static DataManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DataManager(context);
        }
        return sInstance;
    }

    public DataManager(Context context) {
        mImages = readImages(context);
    }

    /**
     * Reading list from filecache (preference).
     * If empty - create new list.
     */
    // TODO move to background thread.
    // Now we don't really need it in case of amount of data, but bad practice anyway.
    private List<ImageItem> readImages(Context context) {
        List<ImageItem> result = null;
        String data = PreferenceManager.getDefaultSharedPreferences(context).getString(DATA_FIELD, null);
        try {
            JSONArray array = new JSONArray(data);
            result = JSONUtil.parseList(array, ImageItem.class);
        } catch (Exception ex) {
            result = null;
        }
        if (result == null) {
            result = loadImages(context);
        }
        return result;
    }

    public List<ImageItem> getImages() {
        return mImages;
    }

    public void setImages(List<ImageItem> images) {
        mImages = images;
    }

    public static void reloadImages(Context context) {
        DataManager instance = getInstance(context);
        instance.setImages(instance.loadImages(context));
    }

    public List<ImageItem> loadImages(Context context) {
        Random random = new Random();
        ArrayList<ImageItem> result = new ArrayList<ImageItem>();
        for (int i = 0; i < Config.IMAGES_COUNT; i++) {
            result.add(ImageItem.createFromRandom(random));
        }
        storeResult(context, result);
        return result;
    }

    // To prevent any ANRs and make smooth UI we are writing to file in background
    private void storeResult(final Context context, final ArrayList<ImageItem> result) {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                String storeString = JSONUtil.toString(result);
                PreferenceManager.getDefaultSharedPreferences(context).edit().putString(DATA_FIELD, storeString).commit();
                return null;
            }
        }.execute();
    }

    public static List<ImageItem> getImages(Context context) {
        return getInstance(context).getImages();
    }
}
